﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;


public class ghostCollision : MonoBehaviour
{
    private GameObject[] Wall;

    public void MoveTo(Vector3 pos, Vector3 rot)
    {
        this.transform.position = pos;
        //GetComponent<NavMeshAgent>().SetDestination(pos);
        this.transform.localEulerAngles = rot;
    }

    void Start()
    {
        Wall = GameObject.FindGameObjectsWithTag("Wall");
        foreach(GameObject Walls in Wall)
        {
            Physics.IgnoreCollision(Walls.GetComponent<Collider>(), GetComponent<Collider>());
        }
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            SceneManager.LoadScene("PlayerWin");
            // Game Over. Ghost Win.
        }
    }
}
