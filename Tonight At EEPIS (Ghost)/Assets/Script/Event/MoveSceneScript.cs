﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading;
using System.IO.Compression;
using System.Security.Cryptography;
using System.Security.AccessControl;
using UnityEngine.UI;


public class MoveSceneScript : MonoBehaviour
{
    public string loadTo;
    public InputField username;
    public InputField password;
    private static readonly byte[] SALT = new byte[] { 0x26, 0xdc, 0xff, 0x00, 0xad, 0xed, 0x7a, 0xee, 0xc5, 0xfe, 0x07, 0xaf, 0x4d, 0x08, 0x22, 0x3c };
    loginID id;

    public struct loginID
    {
        public string username;
        public string password;
    }

    // Start is called before the first frame update
    void Start()
    {
        id = new loginID();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ButtonMoveToScene()
    {
        SceneManager.LoadScene(loadTo);
    }

    public void loginId()
    {
        UdpClient client = new UdpClient(5030);
        IPAddress multicastAddress = IPAddress.Parse("239.0.0.222");
        IPEndPoint sender = new IPEndPoint(IPAddress.Parse("192.168.43.232"), 12345);
        client.JoinMulticastGroup(multicastAddress);
        //id.username = username.text;
        //id.password = password.text;
        string a = username.text;
        string b = password.text;
        byte[] ab = Encoding.ASCII.GetBytes(a);
        byte[] bb = Encoding.ASCII.GetBytes(b);
        client.Send(ab, ab.Length, sender);
        client.Send(bb, bb.Length, sender);
    }

    byte[] getBytesFromID(loginID str)
    {
        int size = Marshal.SizeOf(str);
        byte[] arr = new byte[size];

        IntPtr ptr = Marshal.AllocHGlobal(size);
        Marshal.StructureToPtr(str, ptr, true);
        Marshal.Copy(ptr, arr, 0, size);
        Marshal.FreeHGlobal(ptr);
        return arr;
    }

    public static byte[] Encrypt(byte[] input)
    {
        PasswordDeriveBytes pdb =
          new PasswordDeriveBytes("aaa", // Change this
          new byte[] { 0x43 }); // Change this
        MemoryStream ms = new MemoryStream();
        Aes aes = new AesManaged();
        aes.Key = pdb.GetBytes(aes.KeySize / 8);
        aes.IV = pdb.GetBytes(aes.BlockSize / 8);
        CryptoStream cs = new CryptoStream(ms,
          aes.CreateEncryptor(), CryptoStreamMode.Write);
        cs.Write(input, 0, input.Length);
        cs.Close();
        return ms.ToArray();
    }
    public static byte[] Decrypt(byte[] input)
    {
        PasswordDeriveBytes pdb =
          new PasswordDeriveBytes("aaa", // Change this
          new byte[] { 0x43 }); // Change thi
        MemoryStream ms = new MemoryStream();
        Aes aes = new AesManaged();
        aes.Key = pdb.GetBytes(aes.KeySize / 8);
        aes.IV = pdb.GetBytes(aes.BlockSize / 8);
        CryptoStream cs = new CryptoStream(ms,
          aes.CreateDecryptor(), CryptoStreamMode.Write);
        cs.Write(input, 0, input.Length);
        cs.Close();
        return ms.ToArray();
    }
}
