﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Sockets;
using UnityEngine.UI;
using System.Threading;
using System.IO.Compression;
using System.Security.Cryptography;
using System.Security.AccessControl;
using UnityEngine.SceneManagement;



public class playerMovement : MonoBehaviour
{
    private float movementSpeed = 7.0f;
    private float rotationSpeed = 200.0f;
    public static Client clients;
    float[] temp = { 0, 0, 0, 0, 0, 0  };

    static PacketData data2send;
    byte[] buffer;
    public static float timer = 50;
    public Text text;

    public float speedH = 5.0f;
    public float speedV = 5.0f;

    private float yaw = 0.0f;
    private float pitch = 0.0f;

    public static bool timerStart;

    void Start()
    {
        timerStart = false;
        GameObject go = GameObject.Find("Client");
        clients = go.GetComponent<Client>();
        data2send = new PacketData();
        data2send.timer = false;
        data2send.process = "Start2";
        buffer = getBytes(data2send);
        clients.SendTo(buffer, clients.client);
    }

    void Update()
    {
        if (timerStart == true)
        {
            timer -= Time.deltaTime;
            text.GetComponent<Text>().text = timer.ToString();

            if (timer <= 0)
            {
                data2send.process = "GameOverTimer";
                SceneManager.LoadScene("GhostWin");
                //GameOver
            }
        }

        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(0, 0, Time.deltaTime * movementSpeed);
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(-Time.deltaTime * movementSpeed, 0, 0);
        }

        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(0, 0, -Time.deltaTime * movementSpeed);
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Time.deltaTime * movementSpeed, 0, 0);
        }

        yaw += speedH * Input.GetAxis("Mouse X");
       // pitch -= speedV * Input.GetAxis("Mouse Y");
        transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);

        data2send.process = "Moving2";        
        data2send.Xpos = transform.position.x;
        data2send.Ypos = transform.position.y;
        data2send.Zpos = transform.position.z;
        data2send.Xrot = transform.localEulerAngles.x;
        data2send.Yrot = transform.localEulerAngles.y;
        data2send.Zrot = transform.localEulerAngles.z;

        buffer = getBytes(data2send);

        if (data2send.Xpos != temp[0] || data2send.Ypos != temp[1] || data2send.Zpos != temp[2])
        {
            clients.SendTo(buffer, clients.client);
        }
        if (data2send.Xrot != temp[3] || data2send.Yrot != temp[4] || data2send.Zrot != temp[5])
        {
            clients.SendTo(buffer, clients.client);
        }

        temp[0] = data2send.Xpos;
        temp[1] = data2send.Ypos;
        temp[2] = data2send.Zpos;
        temp[3] = data2send.Xrot;
        temp[4] = data2send.Yrot;
        temp[5] = data2send.Zrot;                
    }

    public struct PacketData
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
        public string process;
        public bool timer;
        public float Xpos;
        public float Ypos;
        public float Zpos;
        public float Xrot;
        public float Yrot;
        public float Zrot;
    }

    byte[] getBytes(PacketData str)
    {
        int size = Marshal.SizeOf(str);
        byte[] arr = new byte[size];

        IntPtr ptr = Marshal.AllocHGlobal(size);
        Marshal.StructureToPtr(str, ptr, true);
        Marshal.Copy(ptr, arr, 0, size);
        Marshal.FreeHGlobal(ptr);
        return arr;
    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "PowerUp")
        {
            StartCoroutine("IncreaseSpeed");
            Destroy(other.gameObject);
        }
    }

    IEnumerator IncreaseSpeed()
    {
        movementSpeed += 5;
        yield return new WaitForSecondsRealtime(10);
        movementSpeed -= 5;
    }
}
