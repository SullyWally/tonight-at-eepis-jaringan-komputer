﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class BundleAsset : MonoBehaviour
{
    [MenuItem("Assets/Create Asset Bundles")]
    static void Build()
    {
        BuildPipeline.BuildAssetBundles(@"C:\Users\sully\Documents\Tugas\Semester 3\Project Jarkom", BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows64);
    }
}

#endif
