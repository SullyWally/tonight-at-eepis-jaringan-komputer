﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading;
using System.IO.Compression;
using System.Security.Cryptography;
using System.Security.AccessControl;
using UnityEngine.UI;

public class LoadSceneScript : MonoBehaviour
{
    public string loadTo;
    public InputField username;
    public InputField password;
    private static readonly byte[] SALT = new byte[] { 0x26, 0xdc, 0xff, 0x00, 0xad, 0xed, 0x7a, 0xee, 0xc5, 0xfe, 0x07, 0xaf, 0x4d, 0x08, 0x22, 0x3c };
    loginID id;

    public struct loginID
    {
        public string username;
        public string password;
    }

    // Start is called before the first frame update
    void Start()
    {
        id = new loginID();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ButtonMoveToScene()
    {
        SceneManager.LoadScene(loadTo);
    }

    public void loginId()
    {
        UdpClient client = new UdpClient(5020);
        IPAddress multicastAddress = IPAddress.Parse("239.0.0.222");
        IPEndPoint sender = new IPEndPoint(IPAddress.Parse("192.168.43.232"), 12345);
        client.JoinMulticastGroup(multicastAddress);
        id.username = username.text ;
        id.password = password.text;
        byte[] data = Encrypt(getBytesFromID(id), password.text);
        client.Send(data, data.Length, sender);
    }

    public static byte[] Encrypt(byte[] plain, string password)
    {
        MemoryStream memoryStream;
        CryptoStream cryptoStream;
        Rijndael rijndael = Rijndael.Create();
        Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(password, SALT);
        rijndael.Key = pdb.GetBytes(32);
        rijndael.IV = pdb.GetBytes(16);
        memoryStream = new MemoryStream();
        cryptoStream = new CryptoStream(memoryStream, rijndael.CreateEncryptor(), CryptoStreamMode.Write);
        cryptoStream.Write(plain, 0, plain.Length);
        cryptoStream.Close();
        return memoryStream.ToArray();
    }

    byte[] getBytesFromID(loginID str)
    {
        int size = Marshal.SizeOf(str);
        byte[] arr = new byte[size];

        IntPtr ptr = Marshal.AllocHGlobal(size);
        Marshal.StructureToPtr(str, ptr, true);
        Marshal.Copy(ptr, arr, 0, size);
        Marshal.FreeHGlobal(ptr);
        return arr;
    }
}
