﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Sockets;
using System.IO;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Globalization;
using UnityEngine.AI;

public class ClientScript : MonoBehaviour
{
    private int portToConnect = 8000;
    private string password;
    private bool socketReady;
    private TcpClient socket;
    private NetworkStream stream;
    private StreamWriter writer;
    private StreamReader reader;
    private CultureInfo culture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
    public playerMovement player;
    public ghostMovement ghost;

    public InputField IPAddress;
    public string IP;
    public string data;
        

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(gameObject);
        GetComponent<humanButton>();
        GetComponent<ghostButton>();
    }

    // Update is called once per frame
    void Update()
    {
        if (socketReady)
        {
            if (stream.DataAvailable)
            {
                data = reader.ReadLine();               
                if (data != null)
                {
                    OnIncomingData(data);
                }              
            }
        }
    }

    private void OnIncomingData(string data)
    {
        Debug.Log(data);
        
        switch (data)
        {
            case "Human":
                if(data == humanButton.role1)
                {
                    Debug.Log("You picked Human");
                }
                else
                {
                    Debug.Log("Enemy Picked " + data);
                }
                break;

            case "Ghost":
                if (data == ghostButton.role2)
                {
                    Debug.Log("You picked Ghost");
                }
                else
                {
                    Debug.Log("Enemy Picked " + data);
                }
                break;
            case "SendingAssets":
                SceneManager.LoadScene("MainGame");
                ReceiveAsset();
                break;        
                
            default:
                Debug.Log(data);
                Debug.Log("Unrecognizable command received");
                break;
        }
        
    }

    public void ReceiveAsset()
    {

    }

    public void Send(string data)
    {
        if (!socketReady)
            return;

        writer.WriteLine(data);
        writer.Flush();
    }

    public bool StartConnectToServer(string ip, int port)
    {
        if (socketReady)
            return false;

        try
        {
            socket = new TcpClient(ip, port);
            stream = socket.GetStream();
            writer = new StreamWriter(stream);
            reader = new StreamReader(stream);
            socketReady = true;
            Debug.Log("Connected to Server");        
        }
        catch (Exception e)
        {
            Debug.Log("Socket error " + e.Message);
        }

        return socketReady;
    }

    public void ConnectToServer()
    {
        try
        {
            StartConnectToServer(IP, portToConnect);
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
    }
}
