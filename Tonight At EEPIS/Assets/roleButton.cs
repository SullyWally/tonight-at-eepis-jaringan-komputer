﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class roleButton : MonoBehaviour
{
    private GameObject data;
    private GameObject client;

    // Start is called before the first frame update
    void Start()
    {
        data = GameObject.FindGameObjectWithTag("RoleManager");
        client = GameObject.FindGameObjectWithTag("ServerHandler");
    }

    public void SetHuman()
    {

        data.GetComponent<roleData>().role = "Human";
        client.GetComponent<ClientScript>().Send("Human");
        

    }
    public void SetGhost()
    {
        data.GetComponent<roleData>().role = "Ghost";
        client.GetComponent<ClientScript>().Send("Human");
        
    }
}
