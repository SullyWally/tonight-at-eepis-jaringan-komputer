﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class buttonScript : MonoBehaviour
{
    public InputField IP_Add;
    
    private GameObject data;
    private GameObject Client;

    // Start is called before the first frame update
    void Start()
    {
        data = GameObject.FindGameObjectWithTag("RoleManager");
        Client = GameObject.FindGameObjectWithTag("ServerHandler");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void addIP()
    {
        data.GetComponent<roleData>().IPaddress = IP_Add.text;
        Client.GetComponent<ClientScript>().IP = IP_Add.text;
        Client.GetComponent<ClientScript>().ConnectToServer();
        SceneManager.LoadScene("Menu");
    }
}
