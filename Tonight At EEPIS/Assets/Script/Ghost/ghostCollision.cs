﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ghostCollision : MonoBehaviour
{
    private GameObject[] Wall;
    private float movementSpeed = 1.5f;
    private float rotationSpeed = 200.0f;


    public void MoveTo(Vector3 pos, Vector3 rot)
    {
        GetComponent<NavMeshAgent>().SetDestination(pos);
        this.transform.localEulerAngles = rot;

    }

    void Start()
    {
        Wall = GameObject.FindGameObjectsWithTag("Wall");
        foreach(GameObject Walls in Wall)
        {
            Physics.IgnoreCollision(Walls.GetComponent<Collider>(), GetComponent<Collider>());
        }
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            // Game Over. Ghost Win.
        }
    }


}
