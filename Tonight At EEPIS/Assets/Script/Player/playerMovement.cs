﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System;

public class playerMovement : MonoBehaviour
{
    private GameObject role;

    private float movementSpeed = 2.0f;
    private float rotationSpeed = 200.0f;

    public float speedH = 5.0f;
    public float speedV = 5.0f;

    private float yaw = 0.0f;
    private float pitch = 0.0f;

    private GameObject[] Wall;
    private Light playerLight;
    private Light EnemyLight;
    
    public static Client clients;
    public int clientNum = 1;
    float[] temp = { 0, 0, 0 ,0 ,0 ,0};
    static PacketData data2send;
    byte[] buffer;

    private void Start()
    {
        GameObject go = GameObject.Find("Client");
        clients = go.GetComponent<Client>();
        role = GameObject.FindGameObjectWithTag("RoleManager");
        playerLight = GameObject.FindGameObjectWithTag("playersLight").GetComponent<Light>();
        EnemyLight = GameObject.FindGameObjectWithTag("EnemyLight").GetComponent<Light>();
        if (role.GetComponent<roleData>().role == "Ghost")
        {
            movementSpeed = 1.5f;
            Wall = GameObject.FindGameObjectsWithTag("Wall");
            foreach (GameObject Walls in Wall)
            {
                Physics.IgnoreCollision(Walls.GetComponent<Collider>(), GetComponent<Collider>());
            }
            playerLight.type = LightType.Point;
            playerLight.range = 5;
            EnemyLight.type = LightType.Spot;
            EnemyLight.range = 10;
            data2send.process = "Moving2";
        }
        if (role.GetComponent<roleData>().role == "Human")
        {
            movementSpeed = 2.0f;
            playerLight.type = LightType.Spot;
            playerLight.range = 10;
            EnemyLight.type = LightType.Point;
            EnemyLight.range = 5;
            data2send.process = "Moving1";
        }
    }
    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(0, 0, Time.deltaTime * movementSpeed);
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(-Time.deltaTime * movementSpeed, 0, 0);
        }
        
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(0, 0, -Time.deltaTime * movementSpeed);
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Time.deltaTime * movementSpeed,0, 0);
        }

        yaw += speedH * Input.GetAxis("Mouse X");
        pitch -= speedV * Input.GetAxis("Mouse Y");
        transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
               
        data2send.num = clientNum;
        data2send.Xpos = transform.position.x;
        data2send.Ypos = transform.position.y;
        data2send.Zpos = transform.position.z;
        data2send.Xrot = transform.localEulerAngles.x;
        data2send.Yrot = transform.localEulerAngles.y;
        data2send.Zrot = transform.localEulerAngles.z;
        
        buffer = getBytes(data2send);
        
        if (data2send.Xpos != temp[0] || data2send.Ypos != temp[1] || data2send.Zpos != temp[2])
        {
            clients.SendTo(buffer, clients.client);
        }
        if (data2send.Xrot != temp[3] || data2send.Yrot != temp[4] || data2send.Zrot != temp[5])
        {
            clients.SendTo(buffer, clients.client);
        }

        temp[0] = data2send.Xpos;
        temp[1] = data2send.Ypos;
        temp[2] = data2send.Zpos;
        temp[3] = data2send.Xrot;
        temp[4] = data2send.Yrot;
        temp[5] = data2send.Zrot;
    }

	  public struct PacketData
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
        public string role;
        public string process;
        public int num;
        public float Xpos;
        public float Ypos;
        public float Zpos;
        public float Xrot;
        public float Yrot;
        public float Zrot;
    }

    byte[] getBytes(PacketData str)
    {
        int size = Marshal.SizeOf(str);
        byte[] arr = new byte[size];

        IntPtr ptr = Marshal.AllocHGlobal(size);
        Marshal.StructureToPtr(str, ptr, true);
        Marshal.Copy(ptr, arr, 0, size);
        Marshal.FreeHGlobal(ptr);
        return arr;
    }
}
