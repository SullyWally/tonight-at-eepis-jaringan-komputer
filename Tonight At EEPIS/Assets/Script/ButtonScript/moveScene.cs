﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class moveScene : MonoBehaviour
{
    public string scene;

    public void MoveScene()
    {
        SceneManager.LoadScene(scene);
    }
}
