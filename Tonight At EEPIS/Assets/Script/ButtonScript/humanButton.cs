﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Runtime.InteropServices;
using System;

public class humanButton : MonoBehaviour
{
    private GameObject data;
    private GameObject client;
    public static string role1 = "a";
    public static Client clients;
    byte[] buffer;

    private void Start()
    {
        data = GameObject.FindGameObjectWithTag("RoleManager");
        client = GameObject.FindGameObjectWithTag("ServerHandler");
    }

    private void Update()
    {
        
        if (client.GetComponent<ClientScript>().data == role1)
        {
            gameObject.SetActive(false);
        }        
    }

    public void SetHuman()
    {
        clients.SendTo(buffer, clients.client);
        role1 = "Human";
        data.GetComponent<roleData>().role = role1;
    }

    public struct PacketData
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
        public string role;
        public string process;
        public int num;
        public float Xpos;
        public float Ypos;
        public float Zpos;
        public float Xrot;
        public float Yrot;
        public float Zrot;
    }

    public PacketData fromBytes(byte[] arrData)
    {
        PacketData str = new PacketData();

        int size = Marshal.SizeOf(str);
        IntPtr ptr = Marshal.AllocHGlobal(size);

        Marshal.Copy(arrData, 0, ptr, size);

        str = (PacketData)Marshal.PtrToStructure(ptr, str.GetType());
        Marshal.FreeHGlobal(ptr);

        return str;
    }
}
