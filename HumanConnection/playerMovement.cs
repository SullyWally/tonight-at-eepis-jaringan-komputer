﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System;

public class playerMovement : MonoBehaviour
{
    private float movementSpeed = 2.0f;
    private float rotationSpeed = 200.0f;
    public static Client clients;
    public int clientNum = 1;
    float[] temp = { 0, 0, 0 };
    static PacketData data2send;
    byte[] buffer;

    void Start()
    {
        GameObject go = GameObject.Find("Client");
        clients = go.GetComponent<Client>();
        data2send = new PacketData();
    }

    void Update()
    {
        transform.Rotate(0, Input.GetAxis("Horizontal") * Time.deltaTime * rotationSpeed, 0);
        transform.Translate(0, 0, Input.GetAxis("Vertical") * Time.deltaTime * movementSpeed);

        data2send.process = "Moving1";
        data2send.num = clientNum;
        data2send.Xpos = transform.position.x;
        data2send.Ypos = transform.position.y;
        data2send.Zpos = transform.position.z;

        buffer = getBytes(data2send);
        
        if (data2send.Xpos != temp[0] || data2send.Ypos != temp[1] || data2send.Zpos != temp[2])
        {
            clients.SendTo(buffer, clients.client);
        }

        temp[0] = data2send.Xpos;
        temp[1] = data2send.Ypos;
        temp[2] = data2send.Zpos;
    }

    public struct PacketData
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
        public string process;
        public int num;
        public float Xpos;
        public float Ypos;
        public float Zpos;
    }

    byte[] getBytes(PacketData str)
    {
        int size = Marshal.SizeOf(str);
        byte[] arr = new byte[size];

        IntPtr ptr = Marshal.AllocHGlobal(size);
        Marshal.StructureToPtr(str, ptr, true);
        Marshal.Copy(ptr, arr, 0, size);
        Marshal.FreeHGlobal(ptr);
        return arr;
    }
}
