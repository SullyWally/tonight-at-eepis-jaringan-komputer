﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Sockets;
using System.IO;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Globalization;
using UnityEngine.AI;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Threading;

public class Client : MonoBehaviour
{   
    private string password;
    private bool socketReady = false;
    private int port = 8090; 

    private CultureInfo culture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
    private playerMovement player;
    private ghostMovement ghost;
    public UdpClient client;

    // Start is called before the first frame update
    void Start()
    {
        culture.NumberFormat.NumberDecimalSeparator = ".";
        GameObject pl = GameObject.Find("Player");
        player = pl.GetComponent<playerMovement>();
        GameObject go = GameObject.Find("Ghost");
        ghost = go.GetComponent<ghostMovement>();

        client = new UdpClient(port);
        ConnectToServer("127.0.0.1", port);
    }

    // Update is called once per frame   
    void Update()
    {        
        
    }

    public void SendTo(byte[] dat, UdpClient client)
    {
        if (!socketReady)
        {
            return;
        }

        IPAddress multicastAddress = IPAddress.Parse("239.0.0.222");
        IPEndPoint sender = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 12345);        
        client.Send(dat, dat.Length, sender);        
    }
    
    public bool ConnectToServer(string host, int port)
    {
        if (socketReady)
            return false;

        try
        {            
            IPAddress multicastAddress = IPAddress.Parse("239.0.0.222");
            client.JoinMulticastGroup(multicastAddress);            
            socketReady = true;
            //SendTo("start", client);
            Debug.Log("Start!!!");
        }
        catch (Exception e)
        {
            Debug.Log("Socket error " + e.Message);
        }

        return socketReady;
    }

    public void ConnectToServerButton()
    {
        try
        {
            ConnectToServer("127.0.0.1", port);
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
    }
}
