﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.IO;
using System.Globalization;
using System.Net;
using System.Runtime.InteropServices;

public static class globalVar
{
    public static PacketData data2recv;
    public struct PacketData
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
        public string process;
        public int num;
        public float Xpos;
        public float Ypos;
        public float Zpos;
    }
}

public class ghostMovement : MonoBehaviour
{
    public ghostCollision ghost;

    UdpClient client;    
    private CultureInfo culture = (CultureInfo)CultureInfo.CurrentCulture.Clone();


    private void Start()
    {
        GameObject go = GameObject.Find("Ghost");
        ghost = go.GetComponent<ghostCollision>();

        culture.NumberFormat.NumberDecimalSeparator = ".";
        client = new UdpClient(8050);
        IPAddress multicastAddress = IPAddress.Parse("239.0.0.222");        
        client.JoinMulticastGroup(multicastAddress);
        Debug.Log("receiver start");                        
        client.BeginReceive(new AsyncCallback(processDgram), client);             
    }


    public void processDgram(IAsyncResult res)
    {
        IPEndPoint recv = new IPEndPoint(IPAddress.Any, 12345);
        try
        {                        
            byte[] recieved = client.EndReceive(res, ref recv);
            globalVar.data2recv = fromBytes(recieved);
            client.BeginReceive(new AsyncCallback(processDgram), client);           
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }  

    void Update()
    {
        OnIncomingData(globalVar.data2recv);
    }

    public void OnIncomingData(globalVar.PacketData dataRecv)
    {
        if (dataRecv.process != null)
        {
            switch (dataRecv.process)
            {
                case "ConnectedToServer":
                    Debug.Log("Start");
                    break;

                case "Moving1":
                    Debug.Log("Pos : " + " x:" + dataRecv.Xpos + " y:" + dataRecv.Ypos + " z:" + dataRecv.Zpos);
                    ghost.MoveTo(new Vector3(dataRecv.Xpos, dataRecv.Ypos, dataRecv.Zpos));
                    Debug.Log("move Player");
                    break;
                default:
                    Debug.Log("Unrecognizable command received");
                    break;
            }
        }
        else
        {
            Debug.Log("NoData");
        }
    }

    public globalVar.PacketData fromBytes(byte[] arrData)
    {
        globalVar.PacketData str = new globalVar.PacketData();

        int size = Marshal.SizeOf(str);
        IntPtr ptr = Marshal.AllocHGlobal(size);

        Marshal.Copy(arrData, 0, ptr, size);

        str = (globalVar.PacketData)Marshal.PtrToStructure(ptr, str.GetType());
        Marshal.FreeHGlobal(ptr);

        return str;
    }
}
