﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading;
using System.IO.Compression;
using System.Security.Cryptography;
using System.Security.AccessControl;

namespace Server
{
    static class Program
    {
        public static ServerAction server;       

        [STAThread]
        static void Main(string[] args)
        {
            CultureInfo customculture = (CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customculture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customculture;                 
            server = new ServerAction();                        
        }        

        
    } 


    ////--------------------MAIN GAME CONNECTION---------------------------//
    public class ServerAction
    {
        private int phuman = 8060;
        private int pghost = 8050;
        private bool serverStarted = true;        
        UdpClient client = new UdpClient(12345);
        UdpClient client2 = new UdpClient(10020);
        IPAddress multicastAddress = IPAddress.Parse("239.0.0.222");
        int timerReady = 0;
        int clientLogin = 0;
        private static readonly byte[] SALT = new byte[] { 0x26, 0xdc, 0xff, 0x00, 0xad, 0xed, 0x7a, 0xee, 0xc5, 0xfe, 0x07, 0xaf, 0x4d, 0x08, 0x22, 0x3c };

        public ServerAction()
        {
            try
            {                
                client.JoinMulticastGroup(multicastAddress);
                client2.JoinMulticastGroup(multicastAddress);

                IPEndPoint recv = null;

                Console.WriteLine("Start Server");                               
                //Console.WriteLine("Client 1 Connected! " + data);

                while (true)
                {                    
                    OnIncomingData(client, recv);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Error Connection.!");
                Console.ReadLine();
            }
        }       

        // Server Read
        
        private void OnIncomingData(UdpClient clients, IPEndPoint rec)
        {
            if (!serverStarted)
            {
                return;
            }

            if (clientLogin < 2)
            {
                byte[] data = client.Receive(ref rec);
                byte[] pass = client.Receive(ref rec);
                clientLogin++;
                string a = Encoding.ASCII.GetString(pass);
                byte[] b = Decrypt(data);
                string username = Encoding.ASCII.GetString(b);
                Console.WriteLine("Client Username : " + username);
            }

            else if(clientLogin >= 2 )
            {
                byte[] data = client.Receive(ref rec);                
                PacketData data1 = fromBytes(data);

                Console.WriteLine("Received :");

                if(timerReady == 2)
                {
                    data1.timer = true;
                    data = getBytes(data1);
                }                
                
                if(data1.process != null)
                {
                    switch (data1.process)
                    {
                        case "Start1":
                            Console.WriteLine("start " + data);
                            timerReady += 1;
                            break;
                        case "Start2":
                            Console.WriteLine("start " + data);
                            timerReady += 1;
                            break;
                        case "Moving1":
                            SendTo(data, true);
                            Console.WriteLine("client 1 : " + "x:" + data1.Xpos + "y:" + data1.Ypos + "z:" + data1.Zpos);
                            break;
                        case "Moving2":
                            SendTo(data, false);
                            Console.WriteLine("client 2 : " + "x:" + data1.Xpos + "y:" + data1.Ypos + "z:" + data1.Zpos);
                            break;
                        case "GameOverTimer":
                            SendTo(data, false);
                            SendTo(data, true);
                            break;
                        default:
                            Console.WriteLine("NoData");
                            break;
                    }
                }                                            
            }                        
        }

        public void SendTo(byte[] dat, bool to)
        {       
            Console.WriteLine("Send");
            IPEndPoint remoteEndPoint = new IPEndPoint(IPAddress.Parse("192.168.43.206"), phuman);
            //IPEndPoint remoteEndPoint = new IPEndPoint(multicastAddress, phuman);
            IPEndPoint remoteEndPoint2 = new IPEndPoint(multicastAddress, pghost);           

            if (to == true)
            {
                Console.WriteLine("Send to 2");
                client.Send(dat, dat.Length, remoteEndPoint2);
            }
            else
            {
                Console.WriteLine("Send to 1");
                client.Send(dat, dat.Length, remoteEndPoint);                
            }                                 
        }

        public struct PacketData
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string process;
            public bool timer;
            public float Xpos;
            public float Ypos;
            public float Zpos;
            public float Xrot;
            public float Yrot;
            public float Zrot;
        }

        public struct loginID
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string username;
            public string password;
        }

        byte[] getBytesFromID(loginID str)
        {
            int size = Marshal.SizeOf(str);
            byte[] arr = new byte[size];

            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(str, ptr, true);
            Marshal.Copy(ptr, arr, 0, size);
            Marshal.FreeHGlobal(ptr);
            return arr;
        }        

        public loginID fromBytesID(byte[] arrData)
        {
            loginID str = new loginID();

            int size = Marshal.SizeOf(str);
            IntPtr ptr = Marshal.AllocHGlobal(size);

            Marshal.Copy(arrData, 0, ptr, size);

            str = (loginID)Marshal.PtrToStructure(ptr, str.GetType());
            Marshal.FreeHGlobal(ptr);

            return str;
        }

        byte[] getBytes(PacketData str)
        {
            int size = Marshal.SizeOf(str);
            byte[] arr = new byte[size];

            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(str, ptr, true);
            Marshal.Copy(ptr, arr, 0, size);
            Marshal.FreeHGlobal(ptr);
            return arr;
        }

        public PacketData fromBytes(byte[] arrData)
        {
            PacketData str = new PacketData();            

            int size = Marshal.SizeOf(str);
            IntPtr ptr = Marshal.AllocHGlobal(size);

            Marshal.Copy(arrData, 0, ptr, size);

            str = (PacketData)Marshal.PtrToStructure(ptr, str.GetType());
            Marshal.FreeHGlobal(ptr);
       
            return str;
        }

        public static byte[] Encrypt(byte[] input)
        {
            PasswordDeriveBytes pdb =
              new PasswordDeriveBytes("aaa", // Change this
              new byte[] { 0x43 }); // Change this
            MemoryStream ms = new MemoryStream();
            Aes aes = new AesManaged();
            aes.Key = pdb.GetBytes(aes.KeySize / 8);
            aes.IV = pdb.GetBytes(aes.BlockSize / 8);
            CryptoStream cs = new CryptoStream(ms,
              aes.CreateEncryptor(), CryptoStreamMode.Write);
            cs.Write(input, 0, input.Length);
            cs.Close();
            return ms.ToArray();
        }

        public static byte[] Decrypt(byte[] input)
        {
            PasswordDeriveBytes pdb =
              new PasswordDeriveBytes("aaa", // Change this
              new byte[] { 0x43 }); // Change this
            MemoryStream ms = new MemoryStream();
            Aes aes = new AesManaged();
            aes.Key = pdb.GetBytes(aes.KeySize / 8);
            aes.IV = pdb.GetBytes(aes.BlockSize / 8);
            CryptoStream cs = new CryptoStream(ms,
              aes.CreateDecryptor(), CryptoStreamMode.Write);
            cs.Write(input, 0, input.Length);
            cs.Close();
            return ms.ToArray();
        }
    }
}
